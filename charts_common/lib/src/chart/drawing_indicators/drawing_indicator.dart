import 'package:charts_common/common.dart';

abstract class DrawingIndicator<T, D> {
  final String id;
  final Color color;
  final Series<T, D> series;

  DrawingIndicator(this.id, this.color, this.series);

  Series calculateIndicatorSeries();
}

abstract class IndicatorSettings {
  final String _id;
  final Color _color;

  String get id => _id;
  Color get color => _color;

  IndicatorSettings(this._id, this._color);
}
