import 'package:charts_common/common.dart';

class MAIndicator<T, D> extends DrawingIndicator<T, D> {
  final MAIndicatorSetting settings;

  MAIndicator(this.settings, Series<T, D> series)
      : super(settings.id, settings.color, series);

  @override
  Series<double, D> calculateIndicatorSeries() {
    double avg = series.data
            .asMap()
            .map((key, value) => MapEntry(key, series.measureFn(key)))
            .values
            .reduce((a, b) => a + b) /
        series.data.length;

    return Series.clone(series as Series<double, D>,
        id: settings.id, seriesColor: color, measureFn: (value, index) => avg)
      ..setAttribute(rendererIdKey, settings.id);
  }
}

class MAIndicatorSetting extends IndicatorSettings {
  MAIndicatorSetting(Color color) : super('MA_indicator', color);
}
