import 'dart:math';

import 'package:charts_common/common.dart';

class SMAIndicator<T, D> extends DrawingIndicator<T, D> {
  final SMAIndicatorSetting settings;

  SMAIndicator(this.settings, Series<T, D> series)
      : super(settings.id, settings.color, series);

  @override
  Series<double, D> calculateIndicatorSeries() {
    int period = _calibratePeriod(settings.period, series.data.length);
    List<double> calculatedValues = [];

    for (int i = period - 1; i < series.data.length; i++) {
      double avg = 0.0;
      for (int j = i; j > (i - period); j--) avg += series.measureFn(j);

      avg = avg / settings.period;
      calculatedValues.add(avg);
    }

    return Series.clone(series as Series<double, D>,
        id: settings.id,
        data: calculatedValues,
        seriesColor: color,
        measureFn: (value, index) => value,
        domainFn: (value, index) => series.domainFn(index + period - 1))
      ..setAttribute(rendererIdKey, settings.id);
  }

  int _calibratePeriod(int period, int dataLength) {
    int newPeriod = period;
    while (dataLength <= newPeriod && newPeriod > 1) newPeriod ~/= 2;
    return newPeriod;
  }
}

class SMAIndicatorSetting extends IndicatorSettings {
  final int period;

  SMAIndicatorSetting(this.period, Color color) : super('SMA_indicator', color);
}
