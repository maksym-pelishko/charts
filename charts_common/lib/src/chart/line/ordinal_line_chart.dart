import 'dart:collection' show LinkedHashMap;
import '../../../common.dart';
import '../cartesian/axis/axis.dart' show NumericAxis;
import '../cartesian/cartesian_chart.dart' show OrdinalCartesianChart;
import '../common/series_renderer.dart' show SeriesRenderer;
import '../layout/layout_config.dart' show LayoutConfig;

class OrdinalLineChart extends OrdinalCartesianChart {
  OrdinalLineChart(
      {bool vertical,
        LayoutConfig layoutConfig,
        NumericAxis primaryMeasureAxis,
        NumericAxis secondaryMeasureAxis,
        LinkedHashMap<String, NumericAxis> disjointMeasureAxes})
      : super(
      vertical: vertical,
      layoutConfig: layoutConfig,
      primaryMeasureAxis: primaryMeasureAxis,
      secondaryMeasureAxis: secondaryMeasureAxis,
      disjointMeasureAxes: disjointMeasureAxes);

  @override
  SeriesRenderer<String> makeDefaultRenderer() {
    return LineRenderer<String>()..rendererId = SeriesRenderer.defaultRendererId;
  }
}
