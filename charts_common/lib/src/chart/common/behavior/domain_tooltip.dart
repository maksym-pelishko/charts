// Copyright 2018 the Charts project authors. Please see the AUTHORS file
// for details.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import 'dart:math';

import 'package:charts_common/common.dart';
import 'package:charts_common/src/chart/common/chart_canvas.dart';
import 'package:charts_common/src/common/graphics_factory.dart';
import 'package:intl/intl.dart';
import 'package:vector_math/vector_math.dart';

import '../../cartesian/cartesian_chart.dart' show CartesianChart;
import '../../layout/layout_view.dart'
    show
        LayoutPosition,
        LayoutView,
        LayoutViewConfig,
        LayoutViewPaintOrder,
        ViewMeasuredSizes;
import '../base_chart.dart' show BaseChart, LifecycleListener;
import '../datum_details.dart' show DatumDetails;
import '../processed_series.dart' show ImmutableSeries;
import '../selection_model/selection_model.dart'
    show SelectionModel, SelectionModelType;
import 'chart_behavior.dart' show ChartBehavior;

/// Chart behavior that monitors the specified [SelectionModel] and shows tooltip
/// for selected data.
///
/// This is typically used for bars to show specific tooltips.
///
/// It is used in combination with SelectNearest to update the selection model
/// and expand selection out to the domain value.
class DomainTooltip<D> implements ChartBehavior<D> {
  final SelectionModelType selectionModelType;

  BaseChart<D> _chart;

  _DomainTooltipLayoutView _view;

  LifecycleListener<D> _lifecycleListener;

  DomainTooltip([this.selectionModelType = SelectionModelType.info]) {
    _lifecycleListener =
        LifecycleListener<D>(onAxisConfigured: _updateViewData);
  }

  void _selectionChanged(SelectionModel selectionModel) {
    _chart.redraw(skipLayout: true, skipAnimation: true);
  }

  void _updateViewData() {
    final selectedDatumDetails =
        _chart.getSelectedDatumDetails(selectionModelType);

    var point;
    for (DatumDetails<D> detail in selectedDatumDetails) {
      if (detail == null) {
        continue;
      }

      final series = detail.series;
      final datum = detail.datum;

      point = _DatumPoint<D>(
          datum: datum,
          domain: detail.domain,
          series: series,
          textStyleSpec: series.insideLabelStyleAccessorFn(detail.index),
          x: detail.chartPosition.x,
          y: detail.chartPosition.y);
      break;
    }

    _view.position = point;
    _view.isPercentage = _chart.behaviors
        .where((element) => element is PercentInjector)
        .isNotEmpty;
  }

  @override
  void attachTo(BaseChart<D> chart) {
    _chart = chart;
    _view = _DomainTooltipLayoutView<D>();

    if (chart is CartesianChart) {
      // Only vertical rendering is supported by this behavior.
      assert((chart as CartesianChart).vertical);
    }

    chart.addView(_view);

    chart.addLifecycleListener(_lifecycleListener);
    chart
        .getSelectionModel(selectionModelType)
        .addSelectionChangedListener(_selectionChanged);
  }

  @override
  void removeFrom(BaseChart chart) {
    chart.removeView(_view);
    chart
        .getSelectionModel(selectionModelType)
        .removeSelectionChangedListener(_selectionChanged);
    chart.removeLifecycleListener(_lifecycleListener);
  }

  @override
  String get role => 'domainTooltip-${selectionModelType.toString()}';
}

class _DomainTooltipLayoutView<D> extends LayoutView {
  final double defaultPadding = 8;
  final double defaultPointerSize = 5;
  final LayoutViewConfig layoutConfig;
  GraphicsFactory _graphicsFactory;
  Rectangle<int> _drawAreaBounds;
  _DatumPoint position;
  bool isPercentage;

  _DomainTooltipLayoutView()
      : layoutConfig = LayoutViewConfig(
            paintOrder: LayoutViewPaintOrder.linePointHighlighter,
            position: LayoutPosition.DrawArea,
            positionOrder: 1);

  Rectangle<int> get drawBounds => _drawAreaBounds;

  @override
  GraphicsFactory get graphicsFactory => _graphicsFactory;

  @override
  set graphicsFactory(GraphicsFactory value) {
    _graphicsFactory = value;
  }

  @override
  ViewMeasuredSizes measure(int maxWidth, int maxHeight) {
    return null;
  }

  @override
  void layout(Rectangle<int> componentBounds, Rectangle<int> drawAreaBounds) {
    _drawAreaBounds = drawAreaBounds;
  }

  @override
  Rectangle<int> get componentBounds => _drawAreaBounds;

  @override
  bool get isSeriesRenderer => false;

  @override
  void paint(ChartCanvas canvas, double animationPercent) {
    if (position == null) return;

    var numberFormat = NumberFormat('#,###.##');

    double value =
        position.series.measureFn(position.series.data.indexOf(position.datum));
    String formattedValue = isPercentage
        ? '${numberFormat.format((value * 100).roundToDouble())}%'
        : numberFormat.format(value);

    TextElement element = graphicsFactory.createTextElement(
        "${position.series.id}\n${position.domain}: $formattedValue");

    element.textStyle = _getTextStyle(graphicsFactory, position.textStyleSpec)
      ..color = Color.white;
    double textWidth = element.measurement.horizontalSliceWidth;
    double textHeight = element.measurement.verticalSliceWidth;
    Color color = Color.fromHex(code: "#555555");
    double boxHeight = textHeight + 2 * defaultPadding;
    double boxWidth = textWidth + 2 * defaultPadding;
    Point barPoint = Point(position.x, position.y);

    TooltipVerticalPosition verticalPosition = TooltipVerticalPosition.top;
    TooltipHorizontalPosition horizontalPosition =
        TooltipHorizontalPosition.center;

    if (barPoint.y < boxHeight + defaultPointerSize)
      verticalPosition = TooltipVerticalPosition.bottom;

    if (barPoint.x < boxWidth / 2)
      horizontalPosition = TooltipHorizontalPosition.right;
    if ((drawBounds.width - barPoint.x) < boxWidth / 2)
      horizontalPosition = TooltipHorizontalPosition.left;

    double angle = _getAngleByPosition(verticalPosition, horizontalPosition);

    double pointerOversize = defaultPointerSize * 2.5;
    List<Point> pointerPoints = [
      _getTooltipCenterPoint(angle, pointerOversize, pointerOversize,
          destPoint: Point(-0.5, 1.0)),
      _getTooltipCenterPoint(angle, pointerOversize, pointerOversize,
          destPoint: Point(0.5, 1.0)),
      barPoint
    ];
    canvas.drawPolygon(points: pointerPoints, fill: color);

    Point tooltipCenter = _getTooltipCenterPoint(angle, boxWidth, boxHeight);
    canvas.drawRRect(
        _getRectangleByCenterPoint(tooltipCenter, boxWidth, boxHeight),
        radius: 5.0,
        fill: color,
        roundTopLeft: true,
        roundTopRight: true,
        roundBottomLeft: true,
        roundBottomRight: true);
    Rectangle textRect =
        _getRectangleByCenterPoint(tooltipCenter, textWidth, textHeight);
    canvas.drawText(element, textRect.left.toInt(), textRect.top.toInt());
  }

  TextStyle _getTextStyle(
      GraphicsFactory graphicsFactory, TextStyleSpec labelSpec) {
    return graphicsFactory.createTextPaint()
      ..color = labelSpec?.color ?? Color.black
      ..fontFamily = labelSpec?.fontFamily
      ..fontSize = labelSpec?.fontSize ?? 12
      ..lineHeight = labelSpec?.lineHeight;
  }

  Point _getTooltipCenterPoint(double radians, double width, double height,
      {bool isRectangular = true, Point destPoint = const Point(0.0, 1.0)}) {
    Vector3 xBase = Vector3(width / 2 + defaultPointerSize, 0, 0);
    Vector3 yBase = Vector3(0, height / 2 + defaultPointerSize, 0);
    Vector3 originPoint = Vector3(position.x, position.y, 1);
    Matrix3 basisMatrix = Matrix3.columns(xBase, yBase, originPoint);

    Vector3 point = Vector3(destPoint.x, -destPoint.y, 1);
    var angleFn = (double angle, double baseOffset) {
      angle %= 2 * pi;
      double offset = baseOffset;
      if (angle < baseOffset - pi / 2) offset -= pi;
      if (angle > baseOffset + pi / 2) offset += pi;
      return (angle - offset) * 2 + offset;
    };
    double realCosV = cos(radians);
    double realSinV = sin(radians);
    bool isCosMoreSin = realCosV.abs() > realSinV.abs();

    double cosV = cos(angleFn(radians, 3 * pi / 2));
    if (isCosMoreSin) cosV = 1 * realCosV / realCosV.abs();
    double sinV = sin(angleFn(radians, pi));
    if (!isCosMoreSin) sinV = 1 * realSinV / realSinV.abs();
    Matrix3 rotationMatrix = Matrix3.identity()
      ..row0 = Vector3(
          isRectangular ? cosV : realCosV, isRectangular ? sinV : realSinV, 0)
      ..row1 = Vector3(-(isRectangular ? sinV : realSinV),
          isRectangular ? cosV : realCosV, 0);

    Vector3 result = basisMatrix * rotationMatrix * point as Vector3;
    return Point(result.x, result.y);
  }

  Rectangle _getRectangleByCenterPoint(
      Point point, double width, double height) {
    Point halfDiagonal = Point(width / 2, height / 2);
    return Rectangle.fromPoints(point - halfDiagonal, point + halfDiagonal);
  }

  double _getAngleByPosition(TooltipVerticalPosition verticalPosition,
      TooltipHorizontalPosition horizontalPosition) {
    assert(
        !(verticalPosition == TooltipHorizontalPosition.center &&
            horizontalPosition == TooltipHorizontalPosition.center),
        "Incorrect tooltip position, cannot be in center");
    double angle = 0;
    int dir;

    if (verticalPosition == TooltipVerticalPosition.top) {
      angle = 0;
      dir = 1;
    }
    if (verticalPosition == TooltipVerticalPosition.bottom) {
      angle = pi;
      dir = -1;
    }
    if (verticalPosition != TooltipHorizontalPosition.center) {
      if (horizontalPosition == TooltipHorizontalPosition.left)
        angle += pi / 4 * dir;
      if (horizontalPosition == TooltipHorizontalPosition.right)
        angle -= pi / 4 * dir;
    } else {
      dir = horizontalPosition == TooltipHorizontalPosition.left ? 1 : -1;
      angle = pi / 2 * dir;
    }
    return angle;
  }
}

enum TooltipVerticalPosition { center, top, bottom }

enum TooltipHorizontalPosition { center, left, right }

class _DatumPoint<D> extends Point<double> {
  final dynamic datum;
  final D domain;
  final ImmutableSeries<D> series;
  final TextStyleSpec textStyleSpec;

  _DatumPoint(
      {this.datum,
      this.domain,
      this.series,
      this.textStyleSpec,
      double x,
      double y})
      : super(x, y);

  factory _DatumPoint.from(_DatumPoint<D> other, [double x, double y]) {
    return _DatumPoint<D>(
        datum: other.datum,
        domain: other.domain,
        series: other.series,
        textStyleSpec: other.textStyleSpec,
        x: x ?? other.x,
        y: y ?? other.y);
  }
}
