import 'package:charts_flutter/flutter.dart' as charts;
import 'package:charts_common/common.dart' as common
    show
        AxisSpec,
        OrdinalCartesianChart,
        RTLSpec,
        Series,
        IndicatorSettings,
        SeriesRendererConfig;
import 'behaviors/chart_behavior.dart' show ChartBehavior;
import 'base_chart.dart' show LayoutConfig;
import 'base_chart_state.dart' show BaseChartState;
import 'cartesian_chart.dart' show CartesianChart;
import 'selection_model_config.dart' show SelectionModelConfig;

class OrdinalLineChart extends CartesianChart<String> {
  OrdinalLineChart(
    List<common.Series> seriesList, {
    bool animate,
    Duration animationDuration,
    common.AxisSpec domainAxis,
    common.AxisSpec primaryMeasureAxis,
    common.AxisSpec secondaryMeasureAxis,
    common.SeriesRendererConfig<String> defaultRenderer,
    List<ChartBehavior> behaviors,
    List<SelectionModelConfig<String>> selectionModels,
    common.RTLSpec rtlSpec,
    LayoutConfig layoutConfig,
    bool defaultInteractions = true,
    List<common.IndicatorSettings> indicators,
  }) : super(
          seriesList
            ..asMap().forEach(
              (index, series) {
                series
                  ..setAttribute(
                      charts.rendererIdKey, index.toString() + '_line');
              },
            ),
          animate: animate,
          animationDuration: animationDuration,
          domainAxis: domainAxis,
          primaryMeasureAxis: primaryMeasureAxis,
          secondaryMeasureAxis: secondaryMeasureAxis,
          defaultRenderer: defaultRenderer,
          customSeriesRenderers: seriesList
              .asMap()
              .keys
              .map((index) => charts.LineRendererConfig<String>(
                  customRendererId: index.toString() + '_line'))
              .toList(),
          behaviors: behaviors,
          selectionModels: selectionModels,
          rtlSpec: rtlSpec,
          layoutConfig: layoutConfig,
          defaultInteractions: defaultInteractions,
          indicators: indicators,
        );

  @override
  common.OrdinalCartesianChart createCommonChart(BaseChartState chartState) {
    // Optionally create primary and secondary measure axes if the chart was
    // configured with them. If no axes were configured, then the chart will
    // use its default types (usually a numeric axis).
    return new common.OrdinalCartesianChart(
      layoutConfig: layoutConfig?.commonLayoutConfig,
      primaryMeasureAxis: primaryMeasureAxis?.createAxis(),
      secondaryMeasureAxis: secondaryMeasureAxis?.createAxis(),
      disjointMeasureAxes: createDisjointMeasureAxes(),
    );
  }
}
